/* eslint-disable no-console */
import axios from 'axios';

/**
 * Mixin de formulario genérico
 * Tiene el objetivo de permitir la reusabilidad
 * de la funcionalidad de envio de leads
 *
 * @mixin FormMixin
 * @version 1.7
 *
 * En la versión 1.7 se espera que el componente de el feedback o rediriga una
 * landing page
 */

export default {
  props: {
    /**
     * Url de envío
     */
    endpoint: {
      type: String,
      default:
        'https://script.google.com/a/wdinamo.com/macros/s/AKfycbzmKOj-QZ5s2GY2LJ7t3R7KZ9YFuHykj_QNZw8QmA/exec'
    },
    /**
     * Título del mail
     */
    mailTitle: {
      type: String,
      default: 'Lead desde drjorgeobregon.com'
    },
    /**
     * Hoja del drive a usar
     */
    sheetDestiny: {
      type: String,
      default: 'Hemorroides'
    },
    /**
     * Orden en el que se muestran los datos en el correo,
     *   lo que no esté incluido aquí es omitido
     */
    formDataOrder: {
      type: [Array, String],
      default() {
        return ['Fecha', 'Nombre', 'Teléfono', 'E-mail', 'Hospital'];
      }
    },
    /**
     * Correo al que se envía este lead
     */
    sendTo: {
      type: String,
      default: 'isaac@wdinamo.com'
    },
    /**
     * Correo al que las respuestas son enviadas
     */
    replyTo: {
      type: String,
      default: ''
    },
    /**
     * Asunto del correo
     */
    subject: {
      type: String,
      default: 'Formulario de contacto drjorgeobregon.com'
    },
    /**
     * Nombre del remitente
     */
    senderName: {
      type: String,
      default: 'Dinamo'
    },
    /**
     * Con copia
     */
    copyTo: {
      type: String,
      default: ''
    },
    /**
     * Copia oculta
     */
    ocultCopyTo: {
      type: String,
      default: 'cesar@wdinamo.com'
    },
    /**
     * Correo al que se envía las notificaciones de fallos.
     */
    debugMail: {
      type: String,
      default: 'cesar@wdinamo.com'
    }
  },
  data: () => ({
    // Estado de carga
    isLoading: false,
    // Bloqueo del botón y evento de envios
    preventSending: false,
    // Envío exitoso
    success: false,
    // Envío fallo
    failed: false,
    // Campo de terminos de privacidad
    TOS: false,
    // Modelo del formulario
    model: {
      Fecha: '',
      Nombre: '',
      Teléfono: '',
      Correo: '',
      Hospital: ''
    }
  }),
  computed: {
    /**
     * El objeto a enviarse, combina las props con el
     * objeto del formulario.
     *
     * @returns {Object} Payload.
     */
    payload() {
      return Object.assign(
        {
          sheetDestiny: this.sheetDestiny,
          formDataOrder: this.formDataOrder,
          sendTo: this.sendTo,
          replyTo: this.replyTo,
          subject: this.subject,
          debugMail: this.debugMail,
          senderName: this.senderName,
          ocultCopyTo: this.ocultCopyTo,
          copyTo: this.copyTo,
          mailTitle: this.mailTitle
        },
        this.model
      );
    }
  },
  mounted() {
    const today = new Date();
    this.model.Fecha = today.toLocaleString('es');
  },
  methods: {
    /**
     * Procesa el resultado de la respuesta.
     *
     * @param {Object} r - La respuesta del script de gdocs.
     * @param {Object} r.result - El resultado del script.
     * @param {Object} r.mailResult.status - El resultado del envío de correo.
     * @param {Object} r.sheet.status - Se guardó en la hoja?
     */
    handleResponse({ result = {}, mailResult = {} } = {}) {
      if (result) {
        return this.isSuccess();
      } else if (mailResult.status) {
        console.warn('No se guardó en el drive, pero se envió al correo');
        return this.isSuccess();
      } else {
        console.error('Algo malo sucede');
        throw new Error('Failing to send to google sheets');
      }
    },
    /**
     * Callback para mostrar resultado de éxito.
     *
     */
    isSuccess() {
      /**
       * Evento de envio exitoso, se puede usar para cerrar modales
       *
       * @event success
       * @type {Event}
       * @argument {Object} payload - La payload
       */
      this.$emit('success', this.payload);
      this.resetForm();
    },
    /**
     * Callback para mostrar error.
     *
     */
    isError() {
      this.$toast.open({
        duration: 5000,
        message: 'Hubo un error enviando tu solicitud. Intentalo más tarde',
        position: 'is-bottom',
        type: 'is-danger'
      });
      /**
       * Evento de error enviando, se puede usar para cerrar modales
       *
       * @event error
       * @type {Event}
       */
      this.$emit('error');
    },
    /**
     * Resetea los datos del formulario.
     *
     */
    resetForm() {
      const today = new Date();
      Object.keys(this.model).forEach(key => {
        if (this.payload[key] instanceof Date) {
          this.model[key] = new Date();
        } else {
          this.model[key] = '';
        }
      });
      this.model.Fecha = today.toLocaleString('es');
      // this.$el.reset()
    },
    /**
     * Convierte los datos del formulario en FormData.
     *
     * @returns {FormData} FormData.
     */
    formatPayload() {
      const formData = new FormData();
      Object.keys(this.payload).forEach(key => {
        if (this.payload[key] instanceof Date) {
          formData.append(key, this.payload[key].toLocaleString('es'));
        } else if (typeof this.payload[key] === 'object') {
          formData.append(key, JSON.stringify(this.payload[key]));
        } else if (this.payload[key]) {
          formData.append(key, this.payload[key]);
        }
      });
      return formData;
    },
    /**
     * Envía el formulario al script de gdocs.
     *
     * @returns {undefined} - Nothing.
     */
    formSend() {
      if (!this.TOS) {
        alert('Tienes que aceptar el aviso de privacidad');
        return;
      }
      this.isLoading = true;
      this.preventSending = true;
      const vm = this;
      const formData = this.formatPayload();
      return axios
        .post(this.endpoint, formData, {
          headers: {
            'Content-Type': 'application/formdata'
          }
        })
        .then(({ data }) => {
          return vm.handleResponse(data);
        });
    }
  }
};
