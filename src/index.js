
/*
    Hamburger menu in mobile.
*/ 
let world = document.querySelector("html");
let toggle = document.querySelector('#hamburger');
let mainNav = document.querySelector('#main-nav');
let customNav = document.querySelector('.custom-nav');

let navItem = document.querySelectorAll('.main-nav .item');

toggle.addEventListener('click', function(e) {
    e.preventDefault();
    world.classList.toggle('no-scroll')
    toggle.classList.toggle('is-active')
    mainNav.classList.toggle('is-active');
    customNav.classList.toggle('is-active');
});


/**
    Add back to header on scrollBehavior
 */
var scrollpos = window.scrollY;
var header = document.getElementById("customNav");

function add_class_on_scroll() {
    header.classList.add("has-back");
}

function remove_class_on_scroll() {
    header.classList.remove("has-back");
}

navItem.forEach( 
    function(item){
        item.addEventListener('click', toggleMobileNav);
    }
)
function toggleMobileNav(){
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
        world.classList.toggle('no-scroll')
        toggle.classList.toggle('is-active')
        mainNav.classList.toggle('is-active');
        customNav.classList.toggle('is-active');
    }
}

window.addEventListener('scroll', function(){ 
    scrollpos = window.scrollY;

    if(scrollpos > 10){
        add_class_on_scroll();
    }
    else {
        remove_class_on_scroll();
    }
    //console.log(scrollpos);
});