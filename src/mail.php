<?php
/**
 * Envio de mail a sirena
 * 
 * @category Script
 * @package  None
 * @author   César Valadez <cesar@wdinamo.com>
 * @license  GPLv3 https://www.gnu.org/licenses/gpl-3.0.en.html
 * @link     https://www.kiainterlomas.grupodaytona.com/
 */

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
      header('Content-type: application/json');
      http_response_code(422);
      $result = ['result' => 'Invalid data'];
      echo json_encode($result);
      die();
};
$data = file_get_contents('php://input');
$data = json_decode($data);
$mailField = 'E-mail';
$postdata = [
  'Nombre' => $data->Nombre,
  'Apellido' => $data->Apellido,
  'E-mail' => $data->{$mailField},
  'Teléfono' => $data->Teléfono,
  'Origen' => 'Web',
  'Industria' => 'vehicle',
  'Campaña' => 'NEW SPORTAGE 2019 TASA 9.99 + SEGURO GRATIS ',
  "Compañía" =>  "Kia Interlomas",
  'Marca' => 'Kia',
  'Modelo' => $data->Modelo,
];

ini_set('display_errors', 1);
error_reporting(E_ALL);
$from = "kia@kiainterlomas.grupodaytona.com";
// $to = "cesar@wdinamo.com";
$to = "kia-interlomas-grupo-daytona-landing@leads.getsirena.com";
$subject = "Sirena - Standard Email";
$message = '';
foreach ($postdata as $key => $value) {
    $message .= "{$key}: {$value}\n";
}
$headers = "From: " . $from;
mail($to, $subject, $message, $headers);
header('Content-type: application/json');
echo json_encode(['result' => 'The email message was sent.']);