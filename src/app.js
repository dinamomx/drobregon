import Vue from '../node_modules/vue/dist/vue.common';
import { CollapseTransition } from 'vue2-transitions';
import FormGeneric from './FormGeneric.vue';
import SliderScroll from './SliderScroll.vue';

// console.log(FormGeneric);
Vue.component(FormGeneric.name, FormGeneric);

Vue.component(CollapseTransition.name, CollapseTransition);
Vue.component(SliderScroll.name, SliderScroll);
Document.prototype.ready = callback => {
  const stateIsReady = state => state === 'interactive' || state === 'complete';
  if (callback && typeof callback === 'function') {
    if (stateIsReady(document.readyState)) {
      return callback();
    }
    document.addEventListener('DOMContentLoaded', () => {
      if (stateIsReady(document.readyState)) {
        return callback();
      }
    });
  }
};
document.ready(() => {
  const app = new Vue({
    el: '#app',
    data: {
      showTerms: false,
      showModel: {
        sportage: false,
        sorento: false
      }
    }
  });

  window.$app = app;
});
